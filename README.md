# yii2-envconfig

## 1. Instalation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

and add next lines to `composer.json`
```json
    "repositories": [
        {
            "url": "https://bitbucket.org/HardTYZ/yii2-envconfig",
            "type": "git"
        }
    ]
```

and add line to require section of `composer.json`

```json
"hardtyz/yii2-envconfig": "*"
```

## Usage

##### Example configuration

In progress