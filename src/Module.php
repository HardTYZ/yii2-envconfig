<?php
namespace hardtyz\envconfig;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'hardtyz\envconfig\controllers';
    public $helperNamespace = 'hardtyz\envconfig\helpers';
    public $modelsNamespace = 'hardtyz\envconfig\models';
    public $defaultRoute = 'default';
    public $check = [];
}