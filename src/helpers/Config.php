<?php

namespace hardtyz\envconfig\helpers;

class Config
{
    public static function merge(array $config, string $prefix = "YII")
    {
        return array_replace_recursive($config, self::get($prefix));
    }

    public static function get(string $prefix = "YII")
    {
        $path = '/tmp/env_cache';
        if(!file_exists($path))
        {

            $keys = getenv();
            $params = array();
            $prefix = $prefix . '_';
            foreach ($keys as $key => $value) {
                if (substr($key, 0, strlen($prefix)) === $prefix) {
                    $params[$key] = $value;
                }
            }
            $result = self::extract($params,$prefix);
            file_put_contents($path, json_encode($result));
            return $result;
        } else {
            return json_decode(file_get_contents($path), true);
        }
    }

    public static function extract(array $keys,$prefix)
    {
        $config = [];
        foreach ($keys as $key => $value) {
            $array = explode('_', $key);
            unset($array[0]); //remove prefix
            $out = array();
            $cur = &$out;
            foreach ($array as $item) {
                $cur = &$cur[$item];
            }
            $cur = $value;
            $config = array_merge_recursive($config, $out);
        }
        return $config;
    }

}